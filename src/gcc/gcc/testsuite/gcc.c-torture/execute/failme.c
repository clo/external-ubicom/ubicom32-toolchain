int __attribute__ ((noinline)) foo (short x, unsigned short y)
{
  return x * y;
}

int main ()
{
  if (foo (-2, 0xffff) != -131999)
    abort();

  exit (0);
}

