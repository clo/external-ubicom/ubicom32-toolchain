#!/bin/bash

######################################################################
#  Build script for ubicom32 toolchain (-elf target)
#
######################################################################

echo 
echo "Building Ubicom32 ELF toolchain"
date
echo 

# Source common variables and functions
. common.sh

# Source config for the build
. config.build

######################################################################
#
# main script starts here
#

# private variables
SET_DATE=
CLEAN_UP_DATE=

# Process command line options
process_cmd_options

BLDDIR=$BLDDIR
RELDIR=$RELDIR

Status "Configuration Parameters"
Info "Sources from  = $SRCDIR"
Info "Build using   = $BLDDIR"
Info "Release to    = $RELDIR"
Info "PATH          = $PATH"

# always use elf toolchain
CROSS_COMPILE=$RELDIR/bin/ubicom32-elf-

# Info Fixing broken cvs install scripts
find $SRCDIR -name install-sh | xargs chmod +x

if [ "$CONFIG_SET_DATE" == "y" ]; then
    SET_DATE=$(date +%Y%m%d)
    CLEAN_UP_DATE="$SRCDIR/gcc/gcc/DATESTAMP.build_all_sav"
    mv $SRCDIR/gcc/gcc/DATESTAMP $CLEAN_UP_DATE
    echo $SET_DATE
 > $SRCDIR/gcc/gcc/DATESTAMP
fi

[ -z $SET_DATE ] || \
Info "Date	= $SET_DATE"

export LD_LIBRARY_PATH="$RELDIR/lib${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}"
Info "LD_LIB_PATH   = $LD_LIBRARY_PATH"

if [ ! -f ../ubi32-toolchain/release/bin/ubicom32-elf-as ]; then
  error_and_exit "Build Ubi32 Binutils before executing this script"
fi

Info "Removing build and release"
rm -rf build release

Info "Copying Ubi32 binutils"
cp -r ../ubi32-toolchain/release . 

######################################################################
# Configure and build host libraries (gmp and mpfr) if needed

if $CONFIG_BUILD_HOST_LIBS; then
    if [ ! -e $RELDIR/lib/libgmp.a ]; then 
      Status "Build Support Library GMP"
      configure_and_build gmp gmp $SRCDIR/gmp-4.3.1/configure --prefix="$RELDIR" \
	--disable-shared
    fi
    if [ ! -e $RELDIR/lib/libmpfr.a ]; then 
      Status "Build Support Library MPFR"
    configure_and_build mpfr mpfr $SRCDIR/mpfr-2.3.1/configure --prefix="$RELDIR" \
        --with-gmp="$RELDIR" --disable-shared
    fi
fi

######################################################################

# set elf target!!
set_target elf

Info "Build Bootstrap GCC"
configure_and_build_toolchain bootstrap $CONFIG_BUILD_ELF_LANGUAGES \
	--without-headers --disable-multilib \
	--disable-libgloss --disable-newlib --disable-libssp || exit 1

Info "Build Newlib"
cd $ROOTDIR
$ROOTDIR/build_newlib.sh  || exit 1

#  Build final GCC and newlib
Info "Build Final GCC with Newlib"
set_target elf
configure_and_build_toolchain final $CONFIG_BUILD_ELF_LANGUAGES \
	--with-newlib --with-headers --enable-multilib --enable-libssp || exit 1


######################################################################
echo 
Status "BUILD COMPLETE"
date

clean_up
######################################################################
